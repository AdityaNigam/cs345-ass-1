#include <iostream>
#include <climits>
using namespace std;

struct Tree
{
	float x1, x2, y1, y2, y;
	Tree *left;
	Tree *right;
	Tree *parent;
}*root = NULL;

struct node
{
	float x1, x2, y1, y2, y;
}A[2];

struct Tree *newTreeNode(node* element) 
{
	Tree *box = new Tree;
	box->x1 = element->x1; box->x2 = element->x2;
	box->y1 = element->y1; box->y2 = element->y2;
	box->y = element->y;
	box->left = NULL;
	box->right = NULL;
	box->parent = NULL;
	return box;
}

struct Tree* insertTreeNode(struct Tree* rootNow, node* element)
{
	static Tree *p;
	Tree *retNode;
	if (rootNow == NULL)  
	{
		retNode = newTreeNode(element);
		retNode->parent = p;
		cout<<"\nret "<<retNode<<"\n";
		if(root == NULL)
	        root = retNode;
	    return retNode;
	}
	
	if (element->x1 <= rootNow->x1) 
	{
	    cout<<"left";
	    p = rootNow;
	    rootNow->left = insertTreeNode(rootNow->left, element);
	}
	else
	{
	    cout<<"right";
	    p = rootNow;
	    rootNow->right = insertTreeNode(rootNow->right, element);
	}
	    
	return rootNow;
}

int main()
{
	A[0].x1 = 1; A[0].x2 = 1; A[0].y1 = 2; A[0].y2 = 4; A[0].y = 4;
	A[1].x1 = 2; A[1].x2 = 2; A[1].y1 = 3; A[1].y2 = 3.9; A[1].y = 3.9;
	
	insertTreeNode(root, &A[0]);
	insertTreeNode(root, &A[1]);
	
	cout << root->x1 << " " <<root->y<<"\n";
	cout << root->right->x1 << " "<<  root->right->parent->y<<"\n";
	return 0;
}


void TravelnDelete(struct Tree* rootNow, float y2)
{
	if (rootNow == NULL)
		return;
	TravelnDelete(rootNow->left, y2);
	if (rootNow->y2 == y2)
		DeleteTreeNode(rootNow);
	TravelnDelete(rootNow->right, y2);
}

void DeleteTreeNode(struct Tree* rootNow)
{
	Tree *Suc, *par;
	if (rootNow->left !=NULL and rootNow->right != NULL)
	{
		Suc = rootNow->right;
		while (Suc->left != NULL)
		{
			Suc = Suc->left;
		}
		rootNow->x1 = Suc->x1; rootNow->x2 = Suc->x2;
		rootNow->y1 = Suc->y1; rootNow->y2 = Suc->y2;
		rootNow->y = Suc->y;
		if (Suc->parent == rootNow)
			rootNow->right = Suc->right;
		else
			Suc->parent->left = Suc->right;
		free(Suc);
	}
	if (rootNow->left == NULL and rootNow->right == NULL)
	{
		Suc = rootNow;
		rootNow = rootNow->parent;
		if (rootNow->left == Suc)
			rootNow->left = NULL;
		if (rootNow->right == Suc)
			rootNow->right = NULL;
		free(Suc);
	}
	if (rootNow->left == NULL and rootNow->right != NULL)
	{
		Suc = rootNow;
		if (rootNow->parent->left == rootNow)
			rootNow->parent->left = rootNow->right;
		if (rootNow->parent->right == rootNow)
			rootNow->parent->right = rootNow->right;
	}
	if (rootNow->left != NULL and rootNow->right == NULL)
	{
		Suc = rootNow;
		if (rootNow->parent->left == rootNow)
			rootNow->parent->left = rootNow->left;
		if (rootNow->parent->right == rootNow)
			rootNow->parent->right = rootNow->left;
	}
}

